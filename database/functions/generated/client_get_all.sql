DROP FUNCTION get_all_client();
CREATE OR REPLACE FUNCTION get_all_client()
RETURNS SETOF client
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    client;

END;

$$ LANGUAGE plpgsql VOLATILE;
