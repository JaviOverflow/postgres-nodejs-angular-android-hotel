CREATE OR REPLACE FUNCTION put_booking(
  v_id INTEGER,
  v_client_id INTEGER,
  v_adult_amount INTEGER,
  v_kid_amount INTEGER,
  v_start_date DATE,
  v_end_date DATE
) RETURNS INTEGER
AS $$

  BEGIN

    UPDATE
      booking
    SET
      client_id = v_client_id,
      adult_amount = v_adult_amount,
      kid_amount = v_kid_amount,
      start_date = v_start_date,
      end_date = v_end_date
    WHERE
      booking.id = v_id;

    RETURN CASE
       WHEN FOUND THEN 1
                  ELSE 0
       END;

  END;

$$ LANGUAGE plpgsql;
