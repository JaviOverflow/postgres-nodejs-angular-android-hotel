DROP FUNCTION get_all_literal();
CREATE OR REPLACE FUNCTION get_all_literal()
RETURNS SETOF literal
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    literal;

END;

$$ LANGUAGE plpgsql VOLATILE;
