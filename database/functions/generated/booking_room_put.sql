CREATE OR REPLACE FUNCTION put_booking_room(
  v_id INTEGER,
  v_id_booking INTEGER,
  v_id_room INTEGER
) RETURNS INTEGER
AS $$

  BEGIN

    UPDATE
      booking_room
    SET
      id_booking = v_id_booking,
      id_room = v_id_room
    WHERE
      booking_room.id = v_id;

    RETURN CASE
       WHEN FOUND THEN 1
                  ELSE 0
       END;

  END;

$$ LANGUAGE plpgsql;
