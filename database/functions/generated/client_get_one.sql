DROP FUNCTION get_one_client(id INTEGER);
CREATE OR REPLACE FUNCTION get_one_client(
  v_id INTEGER
)
RETURNS SETOF client
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    client
  WHERE
    id = v_id
  LIMIT 1;

END;

$$ LANGUAGE plpgsql VOLATILE;
