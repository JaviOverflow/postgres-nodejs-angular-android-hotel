DROP FUNCTION get_all_booking();
CREATE OR REPLACE FUNCTION get_all_booking()
RETURNS SETOF booking
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    booking;

END;

$$ LANGUAGE plpgsql VOLATILE;
