CREATE OR REPLACE FUNCTION post_client(
  v_name TEXT,
  v_surname TEXT,
  v_dni TEXT
) RETURNS SETOF client
AS $$

  DECLARE

    new_row_id INTEGER;

  BEGIN

    INSERT INTO client (id, name, surname, dni)
              VALUES (DEFAULT, v_name, v_surname, v_dni)
              RETURNING id INTO new_row_id;

    RETURN QUERY
    SELECT *
    FROM client
    WHERE client.id = new_row_id;

  END;

$$ LANGUAGE plpgsql;
