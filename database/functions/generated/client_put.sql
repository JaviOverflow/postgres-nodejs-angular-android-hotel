CREATE OR REPLACE FUNCTION put_client(
  v_id INTEGER,
  v_name TEXT,
  v_surname TEXT,
  v_dni TEXT
) RETURNS INTEGER
AS $$

  BEGIN

    UPDATE
      client
    SET
      name = v_name,
      surname = v_surname,
      dni = v_dni
    WHERE
      client.id = v_id;

    RETURN CASE
       WHEN FOUND THEN 1
                  ELSE 0
       END;

  END;

$$ LANGUAGE plpgsql;
