DROP FUNCTION get_all_room();
CREATE OR REPLACE FUNCTION get_all_room()
RETURNS SETOF room
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    room;

END;

$$ LANGUAGE plpgsql VOLATILE;
