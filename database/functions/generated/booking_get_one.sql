DROP FUNCTION get_one_booking(id INTEGER);
CREATE OR REPLACE FUNCTION get_one_booking(
  v_id INTEGER
)
RETURNS SETOF booking
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    booking
  WHERE
    id = v_id
  LIMIT 1;

END;

$$ LANGUAGE plpgsql VOLATILE;
