CREATE OR REPLACE FUNCTION post_booking_room(
  v_id_booking INTEGER,
  v_id_room INTEGER
) RETURNS SETOF booking_room
AS $$

  DECLARE

    new_row_id INTEGER;

  BEGIN

    INSERT INTO booking_room (id, id_booking, id_room)
              VALUES (DEFAULT, v_id_booking, v_id_room)
              RETURNING id INTO new_row_id;

    RETURN QUERY
    SELECT *
    FROM booking_room
    WHERE booking_room.id = new_row_id;

  END;

$$ LANGUAGE plpgsql;
