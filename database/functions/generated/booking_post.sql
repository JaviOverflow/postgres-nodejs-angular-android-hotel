CREATE OR REPLACE FUNCTION post_booking(
  v_client_id INTEGER,
  v_adult_amount INTEGER,
  v_kid_amount INTEGER,
  v_start_date DATE,
  v_end_date DATE
) RETURNS SETOF booking
AS $$

  DECLARE

    new_row_id INTEGER;

  BEGIN

    INSERT INTO booking (id, client_id, adult_amount, kid_amount, start_date, end_date)
              VALUES (DEFAULT, v_client_id, v_adult_amount, v_kid_amount, v_start_date, v_end_date)
              RETURNING id INTO new_row_id;

    RETURN QUERY
    SELECT *
    FROM booking
    WHERE booking.id = new_row_id;

  END;

$$ LANGUAGE plpgsql;
