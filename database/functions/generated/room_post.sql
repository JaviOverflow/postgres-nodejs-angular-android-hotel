CREATE OR REPLACE FUNCTION post_room(
  v_code TEXT,
  v_name INTEGER,
  v_description INTEGER,
  v_amount INTEGER,
  v_adult_capacity INTEGER,
  v_kid_capacity INTEGER,
  v_adult_price NUMERIC,
  v_kid_price NUMERIC
) RETURNS SETOF room
AS $$

  DECLARE

    new_row_id INTEGER;

  BEGIN

    INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price)
              VALUES (DEFAULT, v_code, v_name, v_description, v_amount, v_adult_capacity, v_kid_capacity, v_adult_price, v_kid_price)
              RETURNING id INTO new_row_id;

    RETURN QUERY
    SELECT *
    FROM room
    WHERE room.id = new_row_id;

  END;

$$ LANGUAGE plpgsql;
