DROP FUNCTION get_all_booking_room();
CREATE OR REPLACE FUNCTION get_all_booking_room()
RETURNS SETOF booking_room
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    booking_room;

END;

$$ LANGUAGE plpgsql VOLATILE;
