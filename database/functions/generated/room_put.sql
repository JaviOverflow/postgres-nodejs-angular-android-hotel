CREATE OR REPLACE FUNCTION put_room(
  v_id INTEGER,
  v_code TEXT,
  v_name INTEGER,
  v_description INTEGER,
  v_amount INTEGER,
  v_adult_capacity INTEGER,
  v_kid_capacity INTEGER,
  v_adult_price NUMERIC,
  v_kid_price NUMERIC
) RETURNS INTEGER
AS $$

  BEGIN

    UPDATE
      room
    SET
      code = v_code,
      name = v_name,
      description = v_description,
      amount = v_amount,
      adult_capacity = v_adult_capacity,
      kid_capacity = v_kid_capacity,
      adult_price = v_adult_price,
      kid_price = v_kid_price
    WHERE
      room.id = v_id;

    RETURN CASE
       WHEN FOUND THEN 1
                  ELSE 0
       END;

  END;

$$ LANGUAGE plpgsql;
