CREATE OR REPLACE FUNCTION delete_room (
  v_id INTEGER
)
RETURNS INTEGER
AS $$

DECLARE

BEGIN

  DELETE FROM room WHERE id = v_id;

  RETURN CASE FOUND WHEN TRUE THEN 1 ELSE 0 END;

END;

$$ LANGUAGE plpgsql VOLATILE COST 100;
