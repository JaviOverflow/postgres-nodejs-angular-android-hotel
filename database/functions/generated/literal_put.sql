CREATE OR REPLACE FUNCTION put_literal(
  v_id INTEGER,
  v_es TEXT,
  v_en TEXT,
  v_ca TEXT
) RETURNS INTEGER
AS $$

  BEGIN

    UPDATE
      literal
    SET
      es = v_es,
      en = v_en,
      ca = v_ca
    WHERE
      literal.id = v_id;

    RETURN CASE
       WHEN FOUND THEN 1
                  ELSE 0
       END;

  END;

$$ LANGUAGE plpgsql;
