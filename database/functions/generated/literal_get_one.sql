DROP FUNCTION get_one_literal(id INTEGER);
CREATE OR REPLACE FUNCTION get_one_literal(
  v_id INTEGER
)
RETURNS SETOF literal
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    literal
  WHERE
    id = v_id
  LIMIT 1;

END;

$$ LANGUAGE plpgsql VOLATILE;
