DROP FUNCTION get_one_room(id INTEGER);
CREATE OR REPLACE FUNCTION get_one_room(
  v_id INTEGER
)
RETURNS SETOF room
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    room
  WHERE
    id = v_id
  LIMIT 1;

END;

$$ LANGUAGE plpgsql VOLATILE;
