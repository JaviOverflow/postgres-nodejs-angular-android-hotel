DROP FUNCTION get_one_booking_room(id INTEGER);
CREATE OR REPLACE FUNCTION get_one_booking_room(
  v_id INTEGER
)
RETURNS SETOF booking_room
AS $$

BEGIN

  RETURN QUERY
  SELECT
    *
  FROM
    booking_room
  WHERE
    id = v_id
  LIMIT 1;

END;

$$ LANGUAGE plpgsql VOLATILE;
