CREATE OR REPLACE FUNCTION post_literal(
  v_es TEXT,
  v_en TEXT,
  v_ca TEXT
) RETURNS SETOF literal
AS $$

  DECLARE

    new_row_id INTEGER;

  BEGIN

    INSERT INTO literal (id, es, en, ca)
              VALUES (DEFAULT, v_es, v_en, v_ca)
              RETURNING id INTO new_row_id;

    RETURN QUERY
    SELECT *
    FROM literal
    WHERE literal.id = new_row_id;

  END;

$$ LANGUAGE plpgsql;
