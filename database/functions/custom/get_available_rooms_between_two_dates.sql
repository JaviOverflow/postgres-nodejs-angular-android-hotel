CREATE TYPE room_availability AS (
  id     INTEGER,
  amount INTEGER
);

DROP FUNCTION get_available_rooms_between_two_dates(startdate DATE, enddate DATE );
CREATE OR REPLACE FUNCTION get_available_rooms_between_two_dates(startdate DATE, enddate DATE)
  RETURNS SETOF room_availability
AS $$
DECLARE
  day DATE;
BEGIN

  CREATE TEMP TABLE bookings_per_day (
    id     INTEGER,
    amount INTEGER
  ) ON COMMIT DROP;

  FOR day IN SELECT generate_series(startdate, enddate, '1 day' :: INTERVAL) LOOP
    RAISE NOTICE '%', day;

    INSERT INTO bookings_per_day
      SELECT
        booking_room.id_room,
        count(id_room)
      FROM
        booking
        INNER JOIN booking_room ON booking_room.id_booking = booking.id
      WHERE 0 = 0
            AND startdate <= booking.end_date
            AND enddate >= booking.start_date
      GROUP BY
        (booking_room.id_room);

  END LOOP;

  CREATE TEMP TABLE rooms_capacity ON COMMIT DROP AS
    SELECT
      id,
      amount
    FROM room;

  CREATE TEMP TABLE rooms_availability ON COMMIT DROP AS
    SELECT
      rooms_capacity.id,
      max(rooms_capacity.amount) - coalesce(max(bookings_per_day.amount), 0)
    FROM
      bookings_per_day
      RIGHT JOIN rooms_capacity ON rooms_capacity.id = bookings_per_day.id
    GROUP BY
      rooms_capacity.id;

  RETURN QUERY
  SELECT *
  FROM rooms_availability;

END;

$$ LANGUAGE plpgsql;

SELECT *
FROM get_available_rooms_between_two_dates('1993-08-25', '1993-08-25');

SELECT *
FROM get_available_rooms_between_two_dates('1993-08-21', '1993-08-21');

SELECT *
FROM get_available_rooms_between_two_dates('1993-08-20', '1993-08-21');

SELECT *
FROM room;
