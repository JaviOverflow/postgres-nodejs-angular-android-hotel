DO $$

DECLARE

  id INTEGER;

BEGIN

  INSERT INTO booking (id, client_id, adult_amount, kid_amount, start_date, end_date) VALUES
    (DEFAULT, 1, 2, 3, '1993-08-20', '1993-08-21') RETURNING booking.id INTO id;

  INSERT INTO booking_room (id, id_booking, id_room) VALUES
    (DEFAULT, id, 1);
  INSERT INTO booking_room (id, id_booking, id_room) VALUES
    (DEFAULT, id, 1);
  INSERT INTO booking_room (id, id_booking, id_room) VALUES
    (DEFAULT, id, 1);
  INSERT INTO booking_room (id, id_booking, id_room) VALUES
    (DEFAULT, id, 1);

END;

$$ LANGUAGE plpgsql;