DO $$

DECLARE
  name_id INTEGER;
  description_id INTEGER;
BEGIN

  TRUNCATE TABLE literal RESTART IDENTITY CASCADE;

  -- title
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT, 'Hotel Paraíso', 'Hotel Paradise', 'Hotel Paraïs');

  -- keywords
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT, 'Hotel,Paraiso,habitacion,lujo,vacaciones', 'Hotel,Paradise,room,luxury,vacations', 'Hotel,habitació,luxe,vacacions');

  -- language
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT, 'es', 'en-US', 'ca');

  -- description
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT, 'El mejor hotel de todo el mundo', 'Best hotel in the whole world', 'El millor hotel de tot el món');


  --Descriptions from: http://www.hotelneri.com/ca/habitacions/classic
  INSERT INTO literal (id, es, en, ca) VALUES (DEFAULT, 'Suite', 'Suite', 'Suite') RETURNING id INTO name_id;
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT,
     'Con una armoniosa y relajante decoración en tonos neutros, las habitaciones Deluxe son más espaciosas y tienen vistas al barrio Gótico. Los suelos de madera y los delicados tejidos aportan elegancia confiriendo a su vez un ambiente muy acogedor.',
     'Chic, comfortable and elegant, our Deluxe rooms have more space and are graciously appointed in neutral tones overlooking the Gothic quarter streets. Wooden floors and delicate fabrics offer a welcoming and relaxing atmosphere. Natural stone bathrooms, with bathtub and rain effect, are intimate and peaceful.',
     'Amb una equilibrada i relaxant decoració en tons neutres, les habitacions Deluxe son més espaioses i tenen vistes al barri Gòtic. El terra de fusta i els delicats teixits donen elegància i un ambient molt acollidor. Els banys, fets en pedra natural, amb banyera i efecte pluja, són íntims i acollidors.')
  RETURNING id INTO description_id;
  INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price) VALUES
    (DEFAULT, 'SUI', name_id, description_id, 10, 2, 0, 150.0, 75.0);

  --Descriptions from: http://www.hotelneri.com/ca/habitacions/classic
  INSERT INTO literal (id, es, en, ca) VALUES (DEFAULT, 'Suite Niño', 'Junior Suite', 'Suite Nin Petit') RETURNING id INTO name_id;
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT,
     'Con mayor espacio y confort, las Junior Suites, con zona de lectura, están decoradas en tonos neutros y tienen vistas a las calles del barrio Gótico. Los suelos de madera y los tejidos les aportan elegancia. Los baños, hecho de piedra natural, con bañera, son íntimos y agradables.',
     'Greater space and comfort, Junior Suites with a living area are graciously appointed in neutral tones overlooking the Gothic quarter streets. Wooden floors and delicate fabrics offer a welcoming and relaxing atmosphere. Natural stone bathrooms, with bathtub and rain effect, are intimate and peaceful.',
     'Amb més espai i confort, les habitacions Junior Suite, amb zona de lectura, estan decorades en tons neutres i tenen vistes als carrers del barri Gòtic. Els terres de fusta i els teixits aporten elegància. Els banys, fets de pedra natural, amb banyera, son íntims i acollidors.')
  RETURNING id INTO description_id;
  INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price) VALUES
    (DEFAULT, 'JSUI', name_id, description_id, 5, 2, 1, 200.0, 100.0);

  INSERT INTO literal (id, es, en, ca) VALUES (DEFAULT, 'Doble', 'Double', 'Doble') RETURNING id INTO name_id;
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT,
     'Con mayor espacio y confort, las Junior Suites, con zona de lectura, están decoradas en tonos neutros y tienen vistas a las calles del barrio Gótico. Los suelos de madera y los tejidos les aportan elegancia. Los baños, hecho de piedra natural, con bañera, son íntimos y agradables.',
     'Greater space and comfort, Junior Suites with a living area are graciously appointed in neutral tones overlooking the Gothic quarter streets. Wooden floors and delicate fabrics offer a welcoming and relaxing atmosphere. Natural stone bathrooms, with bathtub and rain effect, are intimate and peaceful.',
     'Amb més espai i confort, les habitacions Junior Suite, amb zona de lectura, estan decorades en tons neutres i tenen vistes als carrers del barri Gòtic. Els terres de fusta i els teixits aporten elegància. Els banys, fets de pedra natural, amb banyera, son íntims i acollidors.')
  RETURNING id INTO description_id;
  INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price) VALUES
    (DEFAULT, 'DBL', name_id, description_id, 25, 2, 1, 75.0, 50.0);

  INSERT INTO literal (id, es, en, ca) VALUES (DEFAULT, 'Doble Vista Mar', 'Double Sea View', 'Doble Vista al Mar') RETURNING id INTO name_id;
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT,
     'Con mayor espacio y confort, las Junior Suites, con zona de lectura, están decoradas en tonos neutros y tienen vistas a las calles del barrio Gótico. Los suelos de madera y los tejidos les aportan elegancia. Los baños, hecho de piedra natural, con bañera, son íntimos y agradables.',
     'Greater space and comfort, Junior Suites with a living area are graciously appointed in neutral tones overlooking the Gothic quarter streets. Wooden floors and delicate fabrics offer a welcoming and relaxing atmosphere. Natural stone bathrooms, with bathtub and rain effect, are intimate and peaceful.',
     'Amb més espai i confort, les habitacions Junior Suite, amb zona de lectura, estan decorades en tons neutres i tenen vistes als carrers del barri Gòtic. Els terres de fusta i els teixits aporten elegància. Els banys, fets de pedra natural, amb banyera, son íntims i acollidors.')
  RETURNING id INTO description_id;
  INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price) VALUES
    (DEFAULT, 'DVM', name_id, description_id, 5, 2, 1, 100.0, 60.0);

  INSERT INTO literal (id, es, en, ca) VALUES (DEFAULT, 'Individual', 'Single', 'Individual') RETURNING id INTO name_id;
  INSERT INTO literal (id, es, en, ca) VALUES
    (DEFAULT,
     'Con mayor espacio y confort, las Junior Suites, con zona de lectura, están decoradas en tonos neutros y tienen vistas a las calles del barrio Gótico. Los suelos de madera y los tejidos les aportan elegancia. Los baños, hecho de piedra natural, con bañera, son íntimos y agradables.',
     'Greater space and comfort, Junior Suites with a living area are graciously appointed in neutral tones overlooking the Gothic quarter streets. Wooden floors and delicate fabrics offer a welcoming and relaxing atmosphere. Natural stone bathrooms, with bathtub and rain effect, are intimate and peaceful.',
     'Amb més espai i confort, les habitacions Junior Suite, amb zona de lectura, estan decorades en tons neutres i tenen vistes als carrers del barri Gòtic. Els terres de fusta i els teixits aporten elegància. Els banys, fets de pedra natural, amb banyera, son íntims i acollidors.')
  RETURNING id INTO description_id;
  INSERT INTO room (id, code, name, description, amount, adult_capacity, kid_capacity, adult_price, kid_price) VALUES
    (DEFAULT, 'IND', name_id, description_id, 20, 1, 0, 75.0, 60.0);

END;
$$ LANGUAGE plpgsql;
