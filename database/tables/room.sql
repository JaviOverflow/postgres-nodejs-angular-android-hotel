DROP TABLE room;
CREATE TABLE room(
  id SERIAL
  ,code TEXT NOT NULL
  ,name INTEGER
  ,description INTEGER
  ,amount INTEGER NOT NULL
  ,adult_capacity INTEGER NOT NULL
  ,kid_capacity INTEGER NOT NULL
  ,adult_price NUMERIC NOT NULL
  ,kid_price NUMERIC NOT NULL

  ,PRIMARY KEY (id)
  ,FOREIGN KEY (name) REFERENCES literal ON DELETE RESTRICT
  ,FOREIGN KEY (description) REFERENCES literal ON DELETE RESTRICT
  ,UNIQUE (code)

  ,CHECK (amount > 0)
  ,CHECK (adult_capacity >= 0)
  ,CHECK (kid_capacity >= 0)
  ,CONSTRAINT at_least_one_capacity_check CHECK (adult_capacity + kid_capacity > 0)
  ,CHECK (adult_price > 0)
  ,CHECK (kid_price > 0)
);
