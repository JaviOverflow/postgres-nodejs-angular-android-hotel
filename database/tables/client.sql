DROP TABLE client;
CREATE TABLE client(
  id SERIAL
  ,name TEXT NOT NULL
  ,surname TEXT
  ,dni TEXT NOT NULL

  ,PRIMARY KEY (id)
  ,UNIQUE (dni)
);