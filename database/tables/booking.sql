DROP TABLE booking CASCADE;
CREATE TABLE booking(
  id SERIAL
  ,client_id INTEGER NOT NULL
  ,adult_amount INTEGER
  ,kid_amount INTEGER
  ,start_date DATE NOT NULL
  ,end_date DATE NOT NULL

  ,PRIMARY KEY (id)

  ,CHECK (adult_amount >= 0)
  ,CHECK (kid_amount >= 0)
  ,CONSTRAINT at_least_one_individual_check CHECK (adult_amount + kid_amount > 0)
  ,CONSTRAINT time_period_check CHECK (start_date <= end_date)
);