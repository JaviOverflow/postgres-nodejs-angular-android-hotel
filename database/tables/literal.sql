CREATE TABLE literal(
  id SERIAL
  ,es TEXT
  ,en TEXT
  ,ca TEXT

  ,PRIMARY KEY (id)
);