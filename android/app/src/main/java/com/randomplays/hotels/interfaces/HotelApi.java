package com.randomplays.hotels.interfaces;

import com.randomplays.hotels.models.Literal;
import com.randomplays.hotels.models.Room;
import com.randomplays.hotels.models.RoomAvailability;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HotelApi {
    @GET("availability")
    Call<List<RoomAvailability>> getRoomAvailabilitiesBetweenDates(
            @Query("startdate") String startdate,
            @Query("enddate") String enddate);

    @GET("rooms")
    Call<List<Room>> getRooms();

    @GET("literals")
    Call<List<Literal>> getLiterals();
}
