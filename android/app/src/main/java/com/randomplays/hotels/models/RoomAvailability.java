package com.randomplays.hotels.models;

public class RoomAvailability {
    public int id;
    public int amount;

    public RoomAvailability(int amount, int id) {
        this.amount = amount;
        this.id = id;
    }
}
