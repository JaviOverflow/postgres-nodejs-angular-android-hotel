package com.randomplays.hotels.models;

public class Date {
    public int year;
    public int month;
    public int day;

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public String toString() {
        return String.format("%d-%d-%d", year, month + 1, day);
    }
}
