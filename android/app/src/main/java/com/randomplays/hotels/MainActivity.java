package com.randomplays.hotels;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.randomplays.hotels.adapters.RoomsAvailabilityCardAdapter;
import com.randomplays.hotels.interfaces.HotelApi;
import com.randomplays.hotels.models.Date;
import com.randomplays.hotels.models.Literal;
import com.randomplays.hotels.models.Room;
import com.randomplays.hotels.models.RoomAvailability;
import com.randomplays.hotels.services.RetrofitService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button mEndDatePickerButton;
    private Button mStartDatePickerButton;
    private Button mButtonClear;
    private Button mButtonFetch;


    public static final String API_URL = "http://10.0.2.2:8000/api/";

    private Dictionary<Integer, Room> mRooms;
    private Dictionary<Integer, Literal> mLiterals;
    private HotelApi mHotelApiService;
    private RoomsAvailabilityCardAdapter mCardAdapter;
    private Date mStartDate;
    private Date mEndDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDateButtons();
        initDates();

        mHotelApiService = RetrofitService.New(HotelApi.class, API_URL);

        fetchRooms();
        fetchLiterals();

        initRoomAvailabilitiesView();

        initClearButton();
        initFetchButton();
    }


    private void initDateButtons() {
        mStartDatePickerButton = (Button) findViewById(R.id.start_date_picker);
        mStartDatePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(mStartDate);
            }
        });

        mEndDatePickerButton = (Button) findViewById(R.id.end_date_picker);
        mEndDatePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(mEndDate);
            }
        });
    }

    private void pickDate(final Date dateBefore) {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        dateBefore.year = year;
                        dateBefore.month = monthOfYear;
                        dateBefore.day = dayOfMonth;
                        updateTextOfDateButtons();
                    }
                },
                dateBefore.year,
                dateBefore.month,
                dateBefore.day

        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void updateTextOfDateButtons() {
        mStartDatePickerButton.setText(String.format("Fecha inicio: %s", mStartDate));
        mEndDatePickerButton.setText(String.format("Fecha fin: %s", mEndDate));
    }

    private void initDates() {

        Calendar now = Calendar.getInstance();
        mStartDate = new Date(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));

        mEndDate = new Date(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH) + 1);

        updateTextOfDateButtons();
    }

    private void fetchLiterals() {
        mLiterals = new Hashtable<Integer, Literal>();
        mHotelApiService
                .getLiterals()
                .enqueue(new Callback<List<Literal>>() {
                    @Override
                    public void onResponse(Call<List<Literal>> call, Response<List<Literal>> response) {
                        for (Literal literal : response.body())
                            mLiterals.put(literal.id, literal);
                    }

                    @Override
                    public void onFailure(Call<List<Literal>> call, Throwable t) {
                    }
                });
    }

    private void fetchRooms() {
        mRooms = new Hashtable<Integer, Room>();
        mHotelApiService
                .getRooms()
                .enqueue(new Callback<List<Room>>() {
                    @Override
                    public void onResponse(Call<List<Room>> call, Response<List<Room>> response) {
                        for (Room room : response.body())
                            mRooms.put(room.id, room);
                    }

                    @Override
                    public void onFailure(Call<List<Room>> call, Throwable t) {
                    }
                });
    }

    private void initRoomAvailabilitiesView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCardAdapter = new RoomsAvailabilityCardAdapter(mRooms, mLiterals);
        recyclerView.setAdapter(mCardAdapter);
    }

    private void initFetchButton() {
        mButtonFetch = (Button) findViewById(R.id.button_fetch);
        mButtonFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHotelApiService
                        .getRoomAvailabilitiesBetweenDates(mStartDate.toString(), mEndDate.toString())
                        .enqueue(new Callback<List<RoomAvailability>>() {
                            @Override
                            public void onResponse(Call<List<RoomAvailability>> call, Response<List<RoomAvailability>> response) {
                                List<RoomAvailability> roomAvailabilities = response.body();
                                mCardAdapter.refresh(roomAvailabilities);
                            }

                            @Override
                            public void onFailure(Call<List<RoomAvailability>> call, Throwable t) {
                            }
                        });
            }
        });
    }

    private void initClearButton() {
        mButtonClear = (Button) findViewById(R.id.button_clear);
        mButtonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCardAdapter.empty();
            }
        });
    }

}
