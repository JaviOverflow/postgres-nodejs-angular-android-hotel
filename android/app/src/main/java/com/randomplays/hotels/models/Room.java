package com.randomplays.hotels.models;

public class Room {
    public int id;
    public String code;
    public int name;
    public int description;
    public int amount;
    public int adult_capacity;
    public int kid_capacity;
    public float adult_price;
    public float kid_price;

    public Room(int id, String code, int name, int description, int amount, int adult_capacity,
                int kid_capacity, float adult_price, float kid_price) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.amount = amount;
        this.adult_capacity = adult_capacity;
        this.kid_capacity = kid_capacity;
        this.adult_price = adult_price;
        this.kid_price = kid_price;
    }
}
