package com.randomplays.hotels.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.randomplays.hotels.R;
import com.randomplays.hotels.models.Literal;
import com.randomplays.hotels.models.Room;
import com.randomplays.hotels.models.RoomAvailability;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class RoomsAvailabilityCardAdapter extends RecyclerView.Adapter<RoomsAvailabilityCardAdapter.ViewHolder> {

    List<RoomAvailability> roomAvailabilities;
    Dictionary<Integer, Room> rooms;
    Dictionary<Integer, Literal> literals;

    public RoomsAvailabilityCardAdapter(Dictionary<Integer, Room> rooms, Dictionary<Integer, Literal> literals) {
        this.roomAvailabilities = new ArrayList<RoomAvailability>();
        this.rooms = rooms;
        this.literals = literals;
    }

    public void refresh(List<RoomAvailability> roomAvailabilities) {
        this.roomAvailabilities = roomAvailabilities;
        notifyDataSetChanged();
    }

    public void empty() {
        roomAvailabilities.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        RoomAvailability roomAvailability = roomAvailabilities.get(i);
        int roomId = roomAvailability.id;
        viewHolder.name.setText(
                String.format("%s [%d disponibles]",
                        literals.get(rooms.get(roomId).name).es,
                        roomAvailability.amount));
        viewHolder.description.setText(
                String.format("Descripción: %s", literals.get(rooms.get(roomId).description).es));
    }

    @Override
    public int getItemCount() {
        return roomAvailabilities.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
        }
    }
}