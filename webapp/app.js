"use strict";
var app = require("./server/routes");

// Start the server
var port = process.env.PORT || 8000;
var server = app.listen(port, function() {
 console.log('Listening on port %d', server.address().port);
});
