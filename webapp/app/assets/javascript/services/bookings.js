angular.module('HotelModule').factory('Bookings', function($resource){
  return $resource('/api/bookings');
});
