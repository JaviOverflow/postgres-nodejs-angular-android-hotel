angular.module('HotelModule').factory('Rooms', function($resource){
  return $resource('/api/rooms');
});
