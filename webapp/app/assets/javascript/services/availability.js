angular.module('HotelModule').factory('Availability', function($resource){
  return $resource('/api/availability');
});
