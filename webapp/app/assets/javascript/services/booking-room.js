angular.module('HotelModule').factory('BookingRooms', function($resource){
  return $resource('/api/booking_rooms');
});
