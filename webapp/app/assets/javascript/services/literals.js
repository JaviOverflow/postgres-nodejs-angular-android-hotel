angular.module('HotelModule').factory('Literals', function($resource){
  return $resource('/api/literals');
});

angular.module('HotelModule').factory('Literal', function($resource){
  return $resource('/api/literals/:id', null, {
    'update' : {method:'PUT'}
  });
});
