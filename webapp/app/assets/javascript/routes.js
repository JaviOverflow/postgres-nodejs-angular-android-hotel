angular.module('HotelModule').config(function ($routeProvider) {
  $routeProvider
      .when('/', {
        templateUrl: "assets/templates/home.html",
        controller: "HomeController"
      })
      .when('/rooms', {
        templateUrl: "assets/templates/rooms.html",
        controller: "RoomsController"
      })
      .when('/backend', {
        templateUrl: "assets/templates/backend.html",
        controller: "BackendController"
      })
      .when('/marketing', {
        templateUrl: "assets/templates/marketing.html",
        controller: "MarketingController"
      })
});