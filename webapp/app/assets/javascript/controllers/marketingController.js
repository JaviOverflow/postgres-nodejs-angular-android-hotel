angular.module('HotelModule').controller('MarketingController',
    function ($scope, Literals, Literal) {
      "use strict";

      $scope.literalType = {
        1: "Title: ",
        2: "Keywords: ",
        3: "Language: ",
        4: "Description: "
      };
      const keywordsId = 2;
      const titleId = 1;
      const langId = 3;
      const descriptionId = 4;

      $scope.literals = [];

      Literals.query(function (rawLiterals) {
        rawLiterals.map(function (literal) {
          if (literal.id <= 4)
            $scope.literals.push(literal);
        })
      });

      $scope.saveLiteral = function (literal) {
        Literal.update({ id : literal.id}, literal);
      };

    });
