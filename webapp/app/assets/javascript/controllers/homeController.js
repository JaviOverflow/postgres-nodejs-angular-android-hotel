angular.module('HotelModule').controller('HomeController',
    function ($scope, $mdDialog, $mdMedia, $route, $location, Availability, Rooms, Literals, Clients, Bookings, BookingRooms) {
      "use strict";

      const language = 'es';

      var startDate = new Date();
      $scope.startDate = startDate;

      var endDate = new Date();
      endDate.setDate(startDate.getDate() + 1);
      $scope.endDate = endDate;

      var rooms = {};
      var literals = {};
      Literals.query(function (rawLiterals) {
        rawLiterals.map(function (literal) {
          literals[literal.id] = literal;
        });

        Rooms.query(function (rawRooms) {
          rawRooms.map(function (room) {
            room.name = literals[room.name][language];
            room.description = literals[room.description][language];
            rooms[room.id] = room;
          });
        });
      });

      var clients = {};
      Clients.query(function (rawClients) {
        rawClients.map(function (client) {
          clients[client.dni] = client;
        })
      });

      var availabilities = [];
      $scope.availabilities = availabilities;

      $scope.queryAvailabilities = function () {
        Availability.query(
            {startdate: $scope.startDate, enddate: $scope.endDate},
            function (rawAvailabilities) {
              availabilities.length = 0;
              rawAvailabilities.map(function (availability) {
                availabilities.push(availability);
                var room = rooms[availability.id];
                availability.room = room;
                room.price = room.adult_capacity * room.adult_price + room.kid_capacity * room.kid_price;
                availability.unitsToBuy = 0;
              });
            });
      };

      $scope.increaseUnitsToBuy = function (availability) {
        if (availability.unitsToBuy < availability.amount)
          availability.unitsToBuy += 1;
      };

      $scope.decreaseUnitsToBuy = function (availability) {
        if (availability.unitsToBuy > 0)
          availability.unitsToBuy -= 1;
      };

      var client = new Clients();
      client.name = "";
      client.surname = "";
      client.dni = "";
      $scope.client = client;

      $scope.errorMessage = [];

      $scope.reservar = function () {
        $scope.errorMessage = [];

        if (!client.name)
          $scope.errorMessage.push("Debes introducir el nombre\n");

        if (!client.dni)
          $scope.errorMessage.push("Debes introducir el DNI\n");

        var atleastOneNonZero = false;
        availabilities.map(function (availability) {
          if (availability.unitsToBuy > 0)
            atleastOneNonZero = true;
        });
        if (!atleastOneNonZero)
          $scope.errorMessage.push("Debes comprar al menos una habitación\n");

        if ($scope.errorMessage.length != 0)
          return;

        function startBooking() {
          if (clients[client.dni]) {
            client.id = clients[client.dni].id;
            book();
          } else {
            Clients.save(client).$promise.then(function (data) {
              client.id = data.id;
              book();
            });
          }
        }

        function book() {
          var booking = new Bookings();
          booking.client_id = client.id;
          booking.adult_amount = 2;
          booking.kid_amount = 0;
          booking.start_date = startDate;
          booking.end_date = endDate;
          Bookings.save(booking).$promise.then(function (data) {
            booking.id = data.id;

            for (var i = 0; i < availabilities.length; i++) {
              var availability = availabilities[i];

              for (var j = 0; j < availability.unitsToBuy; j++) {
                var bookingRoom = new BookingRooms();
                bookingRoom.id_room = availability.id;
                bookingRoom.id_booking = booking.id;
                BookingRooms.save(bookingRoom).$promise.then(function (data) {
                  console.log(data);
                }, function (err) {
                  console.log(err);
                });
              }
            }

            $scope.queryAvailabilities();

            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#content')))
                    .clickOutsideToClose(true)
                    .title('Estado de tu reserva')
                    .textContent('La reserva se realizo con éxito. Disfruta de tus vacaciones!.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Entendido!')
                    .targetEvent(event)
            );

          });
        }

        startBooking();
      };

    });

