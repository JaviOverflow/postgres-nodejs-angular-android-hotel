angular.module('HotelModule').controller('SeoController',
    function ($scope, Literals) {
      "use strict";

      const currentLang = 'ca';

      const keywordsId = 2;
      const titleId = 1;
      const langId = 3;
      const descriptionId = 4;

      $scope.lang = '';
      $scope.title = '';
      $scope.description = '';
      $scope.keywords = '';

      Literals.query(function (literals) {
        literals.map(function (literal) {
          if (literal.id == titleId)
            $scope.title = literal[currentLang];
          else if (literal.id == keywordsId)
            $scope.keywords = literal[currentLang];
          else if (literal.id == langId)
            $scope.lang = literal[currentLang];
          else if (literal.id == descriptionId)
            $scope.description = literal[currentLang];
        })
      })
    });
