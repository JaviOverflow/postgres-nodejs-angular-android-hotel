angular.module('HotelModule').controller('BackendController',
    function ($scope, $mdDialog, Availability, Literals, Clients, Bookings, BookingRooms, Rooms) {
      "use strict";

      var bookings;
      var clients = Clients.query(function (clients) {
        $scope.clients = {};
        clients.map(function (client) {
          $scope.clients[client.id] = client;
        });

        bookings = Bookings.query(function (bookings) {
          $scope.bookings = bookings;

          Rooms.query(function (rawRooms) {
            var rooms = {};
            rawRooms.map(function (room) {
              rooms[room.id] = room;
            });
            BookingRooms.query(function (bookingrooms) {

              for (var i = 0; i < $scope.bookings.length; i++){
                $scope.bookings[i].rooms = {};
                var purchase_id = 0;

                for (var j = 0; j < bookingrooms; j++) {
                  if (bookingrooms[j].id_booking == $scope.bookings[i].id) {
                    purchase_id += 1;
                    $scope.bookings[i].rooms[purchase_id] = rooms[bookingrooms[j].id_room];;
                  }
                }
              }

            });
          });
        });
      });
    });
