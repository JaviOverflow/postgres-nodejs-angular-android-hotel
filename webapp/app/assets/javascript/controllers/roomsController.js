angular.module('HotelModule').controller('RoomsController',
    function ($scope, Rooms, Literals) {
      const language = 'es';

      var rooms = {};
      $scope.rooms = rooms;

      var literals = {};
      Literals.query(function (rawLiterals) {
        rawLiterals.map(function (literal) {
          literals[literal.id] = literal;
        });

        Rooms.query(function (rawRooms) {
          rawRooms.map(function (room) {
            room.name = literals[room.name][language];
            room.description = literals[room.description][language];
            room.price = room.adult_capacity * room.adult_price + room.kid_capacity * room.kid_price;
            rooms[room.id] = room;
          });
        });
      });

    });
