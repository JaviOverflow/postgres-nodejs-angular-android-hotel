"use strict";

var bodyParser = require('body-parser');
var methodOverride = require('method-override');

// Load environment variables defined at '.env' file
require('dotenv').config();

module.exports = function(app, express) {
  // Serve static assets from the app folder. This enables things like javascript
  // and stylesheets to be loaded as expected. Analog to nginx.
  app.use("/", express.static("app/"));

  // Set the view directory, this enables us to use the .render method inside routes
  app.set('views', __dirname + '/../app/views');

  // Parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: true }));

  // Parse application/json
  app.use(bodyParser.json());

  // Some clients doesn't support secondary verbs like PUT, DELETE, PATCH... This
  // enables them to specify the verb on header 'X-HTTP-Method-Override'
  app.use(methodOverride('X-HTTP-Method-Override'))
};
