var db = require('./../pg_db_adapter');
var express = require('express');
var pgp = require('pg-promise')({});
var database = pgp(process.env.DATABASE);

const soapResponseHeader = "<?xml version='1.0' ?>" +
    "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>" +
    '  <SOAP-ENV:Body>' +
    "    <ns1:getRoomAvailabilityBetweenTwoDates xmlns:ns1='http://127.0.0.1/api/availability?wsdl'>" +
    '      <return>' +
    '        <Hotel>' +
    '          <Details>' +
    '            <Name>Hotel Paradise</Name>' +
    '            <Address>Los Olmos Street, 54, London</Address>' +
    '            <URI>http://hotelparadise.com/</URI>' +
    '          </Details>';

const soapResponseFoot = '' +
    '        </Hotel>' +
    '      </return>' +
    '    </ns1:getRoomAvailabilityBetweenTwoDates>' +
    '  </SOAP-ENV:Body>' +
    '</SOAP-ENV:Envelope>';

module.exports = function (app) {
  var availabilityRouter = express.Router();

  availabilityRouter.route('')

      .get(function (request, response) {
        var query = request.query;
        var params = [
          query['startdate'],
          query['enddate']
        ];
        var wsdl = query['wsdl'];
        db.fetchListAndReturn('get_available_rooms_between_two_dates', params, response);
      })

      .post(require('express-xml-bodyparser')({trim: false, explicitArray: false}), function (request, response) {
        var wsdl = request.query['wsdl'];
        if (typeof wsdl === 'undefined')
          response.status(404).send({error: "Maybe you missed query parameter 'wsdl'?"});

        var xmlParams = request.body['soap-env:envelope']['soap-env:body']['ns1:getroomavailabilitybetweentwodates'];
        var params = [
          xmlParams['startdate'],
          xmlParams['enddate']
        ];

        database.func('wsdl_available_rooms_between_two_dates', params)
            .then(function (data) {
              var availabilities = data;

              var result = soapResponseHeader;

              result += "<Availability>";
              data.map(function (item) {
                result += "<Rooms><RoomType>" + item['id'] + "</RoomType><Number>" + item['amount'] + "</Number></Rooms>";
              });
              result += "</Availability>";

              result += soapResponseFoot;
              response.json(result);

            })
            .catch(function (error) {
              response.json(error);
            });
      });

  app.use('/api/availability', availabilityRouter);
};
