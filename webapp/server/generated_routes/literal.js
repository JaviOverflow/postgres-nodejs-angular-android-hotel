var db = require('./../pg_db_adapter');
var express = require('express');

module.exports = function(app) {
    var literalsRouter = express.Router();

    literalsRouter.route('')

        .get(function(request, response) {
            db.fetchListAndReturn('get_all_literal', [], response);
        })

        .post(function(request, response) {
            var body = request.body;
            var params = [
                body['es'],
                body['en'],
                body['ca']
            ];
            db.fetchItemAndReturn('post_literal', params, response);
        });

    literalsRouter.route('/:id')

        .get(function(request, response) {
            var params = [
                request.params.id
            ];
            db.fetchItemAndReturn('get_one_literal', params, response);
        })

        .put(function(request, response) {
            var body = request.body;
            var params = [
                body['id'],
                body['es'],
                body['en'],
                body['ca']
            ];
            db.fetchItemAndReturn('put_literal', params, response);
        })

        .delete(function(request, response) {
             var params = [
                request.params.id
            ];
            db.fetchItemAndReturn('delete_literal', params, response);
        });

    app.use('/api/literals', literalsRouter);
}
