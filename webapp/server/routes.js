"use strict";

var express = require('express');
var app = express();
const fs = require('fs');
const path = require('path');

// Load Express Configuration
require('./expressConfig')(app, express);

// Root route
app.get('/', function(req, res){
  res.sendFile('index.html', {root: app.settings.views});
});

function addRoutesFromDirectory(app, dirpath) {
  fs.readdir(dirpath, function (err, files) {
    files.map(function(file){
      if (path.extname(file) === '.js')
        require(dirpath + file)(app);
    })
  });
}

// Api generated routes routes
const generatedRoutesDir = __dirname + '/generated_routes/';
addRoutesFromDirectory(app, generatedRoutesDir);

// Api custom routes routes
const customRoutesDir = __dirname + '/custom_routes/';
addRoutesFromDirectory(app, customRoutesDir);

module.exports = app;
